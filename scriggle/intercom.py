class CommandManager:
    """
    Manages a set of callbacks.

    This allows any object to send a signal that can be received by any
    other object without the sender having to register the signal
    before objects can connect a handler to it.

    Each command is an attribute of the CommandManager object.  To run
    the handlers connected to a command, just call the attribute:
        command_manager.foo_command()
    The handlers are run in undefined order.  To connect a handler:
        command_manager.foo_command.connect(handler)
    Connecting the same handler multiple times has no effect.  You can
    disconnect a handler by calling ‘disconnect’ on the command:
        command_manager.foo_command.disconnect(handler)
    You can use ‘connect’ as a decorator to connect a multiline handler
    as soon as it is defined:
        @command_manager.foo_command.connect
        def foo_callback():
            ...

    Each command can be disabled.  When it is disabled, it will not call
    its connected handlers.  The command can be disabled by setting its
    ‘enabled’ property to False, and reënabled by setting it back to
    True.  You can register callbacks to be called when the ‘enabled’
    property changes:
        command_manager.foo_command.enabled_connect(handler)
    and disconnect them with enabled_disconnect. As above,
    enabled_connect can be used as a decorator, and repeated
    attempts to attach the same handler are ignored.  The handler is
    called with the new value of ‘enabled’ as its argument when
    ‘enabled’ changes and when the handler is first connected.
    """
    def __getattr__(self, name):
        command = _Command()
        setattr(self, name, command)
        return command


class _Command:
    def __init__(self):
        self.__callbacks = set()
        self.__enabled = True
        self.__enabled_callbacks = set()

    def connect(self, callback):
        self.__callbacks.add(callback)
        return callback

    def disconnect(self, callback):
        self.__callbacks.remove(callback)

    def __call__(self, *args, **kwargs):
        if self.__enabled:
            for callback in self.__callbacks:
                callback(*args, **kwargs)

    def enabled_connect(self, callback):
        self.__enabled_callbacks.add(callback)
        callback(self.__enabled)
        return callback

    def enabled_disconnect(self, callback):
        self.__enabled_callbacks.remove(callback)

    @property
    def enabled(self):
        return self.__enabled

    @enabled.setter
    def enabled(self, value):
        self.__enabled = value
        for callback in self.__enabled_callbacks:
            callback(value)


class StateManager:
    """Manages a set of state variables.

    This allows a bunch of properties to be collected in one place.
    Like CommandManager, callbacks can be connected to a property
    (to be called when it changes) even if the property hasn’t been
    created yet.

    To get or set a state variable, retrieve or assign to its ‘value’
    property:
        state_manager.foo_prop.value = 42
    To connect or disconnect, use its ‘connect’ and ‘disconnect’
    methods:
        cn = state_manager.foo_prop.connect(lambda v: print(v))
        state_manager.foo_prop.disconnect(cn)
    ‘connect’ returns a connection identifier, which is just the
    callback itself.  As with CommandManager, a callback is not added
    if it is already connected to the variable, and callbacks are run
    in an undefined order.  Callbacks receive one parameter, the new
    value of the variable.

    Some section of code has to actually implement the variable.  That
    code will call ‘register’ on the property with the getter function
    and optional setter function as arguments.
    """

    def __getattr__(self, name):
        state = _State()
        super().__setattr__(name, state)
        return state

    def __setattr__(self, name, value):
        raise StateError(
            'tried to assign to the state variable itself – access its value '
            'through the ‘value’ property'
        )


class StateError(Exception):
    pass


class _State:
    def __init__(self):
        self.__callbacks = set()
        self.__getter = None
        self.__setter = None

    def connect(self, callback):
        self.__callbacks.add(callback)
        if self.__getter is not None:
            callback(self.__getter())
        return callback

    def disconnect(self, callback):
        self.__callbacks.remove(callback)

    def register(self, getter, setter=None):
        if self.__getter is not None:
            raise StateError(
                'can’t register getter for the same state variable twice'
            )
        self.__getter = getter
        self.__setter = setter
        value = self.__getter()
        for cb in self.__callbacks:
            cb(value)

    def notify(self):
        value = self.value
        for cb in self.__callbacks:
            cb(value)

    @property
    def value(self):
        if self.__getter is not None:
            return self.__getter()
        else:
            raise StateError('tried to get a variable that has no getter')

    @value.setter
    def value(self, value):
        if self.__setter is not None:
            self.__setter(value)
        else:
            raise AttributeError("can't set attribute")

    def register_for_gobject_property(self, gobject, gobject_prop_name):
        self.register(lambda: getattr(gobject.props, gobject_prop_name),
                      lambda v: setattr(gobject.props, gobject_prop_name, v))
        gobject.connect(f'notify::{gobject_prop_name.replace("_", "-")}',
                        lambda ob, prop: self.notify())

    def control_gobject_property(self, gobject, gobject_prop_name,
                                 changed_signal_name):
        def on_gobject_prop_changed(*args):
            self.value = getattr(gobject.props, gobject_prop_name)
        handler = gobject.connect(changed_signal_name, on_gobject_prop_changed)
        def on_state_changed(value):
            gobject.handler_block(handler)
            setattr(gobject.props, gobject_prop_name, value)
            gobject.handler_unblock(handler)
        self.connect(on_state_changed)


class Intercom:
    """A command manager and a state manager, bundled together."""

    def __init__(self):
        self.__command = CommandManager()
        self.__state = StateManager()

    @property
    def command(self):
        return self.__command

    @property
    def state(self):
        return self.__state
