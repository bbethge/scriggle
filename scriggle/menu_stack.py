from gettext import gettext as _

import gi
from gi.repository import Gtk

from .menu import Menu
from . import menus


class MenuStack(Gtk.Overlay):
    def __init__(self, intercom, menu_revealer, **props):
        """
        Create a new MenuStack.

        This object manages all the menus, any extra panel, and the
        cursor position label.
        """
        super().__init__()
        self.__intercom = intercom
        self.__menu_revealer = menu_revealer
        self.__history = []
        self.__previous_focus = None

        self.__stack = Gtk.Stack(
            transition_type=Gtk.StackTransitionType.OVER_UP_DOWN,
            vhomogeneous=False, **props
        )
        self.add(self.__stack)

        self.__right_menu = menus.Right(intercom, self)
        self.__stack.add(self.__right_menu)

        self.__left_menu = menus.Left(intercom, self)
        self.__stack.add(self.__left_menu)

        self.__position_label = CursorPositionLabel(valign=Gtk.Align.START)
        self.__update_position_label_halign()
        self.connect(
            'direction-changed',
            lambda w, pd: self.__update_position_label_halign()
        )
        self.__position_label.props.no_show_all = True
        self.add_overlay(self.__position_label)

    @property
    def cursor_position(self):
        return self.__position_label.position

    @cursor_position.setter
    def cursor_position(self, position):
        self.__position_label.position = position

    @property
    def right_menu(self):
        return self.__right_menu

    @property
    def left_menu(self):
        return self.__left_menu

    def show_menu_immediately(self, menu):
        """Make ‘menu’ the active menu without animation."""
        if menu.side == Menu.Side.LEFT:
            self.__position_label.show()
        else:
            self.__position_label.hide()
        transition_type = self.__stack.props.transition_type
        self.__stack.props.transition_type = Gtk.StackTransitionType.NONE
        self.__stack.props.visible_child = menu
        self.__stack.props.transition_type = transition_type

    def on_go_back(self):
        # XXX: This makes the assumption that pinned menus have no
        # submenus.
        self.unpin_menu()
        self.__stack.props.transition_type = Gtk.StackTransitionType.UNDER_DOWN
        self.__stack.props.visible_child = self.__history.pop()

    def pin_menu(self, focus_widget):
        self.__previous_focus = self.get_toplevel().get_focus()
        self.__menu_revealer.menu_pinned = True
        focus_widget.grab_focus()

    def unpin_menu(self):
        if self.__previous_focus is not None:
            self.__previous_focus.grab_focus()
            self.__previous_focus = None
        self.__menu_revealer.menu_pinned = False

    def do_direction_changed(self, previous_direction):
        self.__update_position_label_halign()

    def __update_position_label_halign(self):
        # Because the left and right menus do not get reversed when the
        # interface is right-to-left, we also don’t want to reverse
        # which side the position label is on.
        halign = Gtk.Align.END
        if self.get_direction() == Gtk.TextDirection.RTL:
            halign = Gtk.Align.START
        self.__position_label.props.halign = halign

    def add_menu(self, menu):
        self.__stack.add(menu)

    def show_submenu(self, submenu):
        if submenu.focus_widget is not None:
            self.pin_menu(submenu.focus_widget)
        self.__history.append(self.__stack.props.visible_child)
        self.__stack.props.transition_type = Gtk.StackTransitionType.OVER_UP
        self.__stack.props.visible_child = submenu

    def on_language_activated(self, language_list, path):
        self.unpin_menu()

    def key_event(self, event):
        """Handle a key event from the window."""
        return self.__stack.props.visible_child.key_event(event)


class CursorPositionLabel(Gtk.Label):
    def __init__(self, **props):
        super().__init__(**props)
        self.position = 0, 0

    @property
    def position(self):
        """A pair containing the zero-based line and column number."""
        return self.__line, self.__column

    @position.setter
    def position(self, position):
        self.__line, self.__column = position
        self.props.label = _('Line {0}, Column {1}').format(
            self.__line + 1, self.__column + 1
        )
