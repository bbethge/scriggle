from gettext import gettext as _

import gi
from gi.repository import GLib, Gdk, Gtk, GtkSource

from .menu import Menu, make_menu_class


class Left(Menu):
    def __init__(self, intercom, menu_stack):
        super().__init__(Menu.Side.LEFT, intercom, menu_stack)
        undo = self.bind_key_to_action('z', _('Undo'), 'undo')
        @intercom.command.undo.enabled_connect
        def on_set_can_undo(can_undo):
            undo.props.sensitive = can_undo
        self.bind_key_to_action('x', _('Cut'), 'cut')
        self.bind_key_to_action('c', _('Copy'), 'copy')
        self.bind_key_to_action('v', _('Paste'), 'paste')
        self.bind_key_to_action(
            'e', '↑', 'up', _('Move the cursor up'), repeat=True
        )
        self.bind_key_to_action(
            's', '←', 'left', _('Move the cursor left'), repeat=True
        )
        self.bind_key_to_action(
            'd', '↓', 'down', _('Move the cursor down'), repeat=True
        )
        self.bind_key_to_action(
            'f', '→', 'right', _('Move the cursor right'), repeat=True
        )
        self.bind_key_to_action(
            'w', _('← Word'), 'left_word',
            _('Move the cursor left by a word'), repeat=True
        )
        self.bind_key_to_action(
            'r', _('→ Word'), 'right_word',
            _('Move the cursor right by a word'), repeat=True
        )
        self.bind_key_to_submenu(
            't', _('Selection…'), Selection(intercom, menu_stack),
            _('Commands to change the selection')
        )
        self.add_unused_keys()


class Selection(Menu):
    def __init__(self, intercom, menu_stack):
        super().__init__(Menu.Side.LEFT, intercom, menu_stack)
        self.bind_key_to_back_button('q')
        self.bind_key_to_action(
        	'w', '↧ Selection', 'selection_start_down',
        	_('Contract selection at the start by one line'), repeat=True
    	)
        self.bind_key_to_action(
            'e', '⤒ Selection', 'selection_start_up',
            _('Extend the selection up one line'), repeat=True
        )
        self.bind_key_to_action(
        	'r', 'Select All', 'select_all', _('Select the entire file')
    	)
        self.bind_key_to_action(
            'a', '⇤ Selection', 'selection_start_left',
            _('Move the selection start to the left'), repeat=True
        )
        self.bind_key_to_action(
            's', '↦ Selection', 'selection_start_right',
            _('Move the selection start to the right'),
            repeat=True
        )
        self.bind_key_to_action(
            'd', 'Selection ↤', 'selection_end_left',
            _('Move the selection end to the left'),
            repeat=True
        )
        self.bind_key_to_action(
            'f', 'Selection ⇥', 'selection_end_right',
            _('Move the selection end to the right'), repeat=True
        )
        self.bind_key_to_action(
        	'x', '↥ Selection', 'selection_end_up',
        	_('Contract the selection at the end by one line'), repeat=True
    	)
        self.bind_key_to_action(
            'c', '⤓ Selection', 'selection_end_down',
            _('Extend the selection down one line'), repeat=True
        )
        self.add_unused_keys()


class Right(Menu):
    def __init__(self, intercom, menu_stack):
        super().__init__(Menu.Side.RIGHT, intercom, menu_stack)
        self.bind_key_to_submenu(
            'y', _('Style…'), Style(intercom, menu_stack),
            _('Options related to code style')
        )
        self.bind_key_to_action(
            'n', _('New'), 'new', _('Create a new document')
        )
        self.bind_key_to_action(
            'i', _('Close'), 'close', _('Close the current document')
        )
        self.bind_key_to_action('o', _('Open…'), 'open')
        self.bind_key_to_submenu(
            'j', _('Find…'), Find(intercom, menu_stack), _("Find and replace")
        )
        self.bind_key_to_action('k', _('Save'), 'save')
        self.add_unused_keys()


class Style(Menu):
    def __init__(self, intercom, menu_stack):
        super().__init__(Menu.Side.RIGHT, intercom, menu_stack)
        self.bind_key_to_back_button('bracketright')
        self.bind_key_to_toggle(
            'y', _('Match Brackets'), 'highlight_matching_brackets',
            _('When the cursor is next to a bracket, highlight its matching '
              'counterpart')
        )
        tab_width_selector = Gtk.SpinButton(
            adjustment=Gtk.Adjustment(8, 2, 8, 1, 2, 0)
        )
        intercom.state.tab_width.control_gobject_property(
            tab_width_selector, 'value', 'value_changed'
        )
        # XXX: The menu was pinned by the MenuStack when it showed the
        # style menu and found it had a focus widget.  This seems too
        # complicated.
        tab_width_selector.connect(
            'activate', lambda selector: self._stack.unpin_menu()
        )
        self.add_extra_widget(tab_width_selector, 8, 0, 4, 1)
        self.bind_key_to_widget('u', _('Tab Width:'), tab_width_selector)
        indent_width = Gtk.SpinButton(
            adjustment=Gtk.Adjustment(8, 2, 8, 1, 2, 0)
        )
        intercom.state.indent_width.control_gobject_property(
            indent_width, 'value', 'value_changed'
        )
        indent_width.connect('activate', lambda iw: self._stack.unpin_menu())
        self.add_extra_widget(indent_width, 16, 0, 4, 1)
        self.bind_key_to_widget('o', _('Indent Width:'), indent_width)
        self.bind_key_to_toggle('h', _('Line Numbers'), 'line_numbers')
        self.bind_key_to_submenu(
            'j', _('Language…'), Language(intercom, menu_stack),
            _('Set the computer language to highlight syntax for')
        )
        self.bind_key_to_toggle(
            'k', _('Use Spaces'), 'use_spaces',
            _('Whether to indent with spaces instead of tabs')
        )
        self.bind_key_to_toggle(
            'semicolon', _('Highlight Current Line'), 'highlight_current_line'
        )
        self.bind_key_to_toggle('apostrophe', _('Soft Wrap Lines'), 'soft_wrap')
        self.bind_key_to_toggle(
            'comma', _('Auto Indent'), 'auto_indent',
            _('Whether to copy the indentation from the previous line')
        )
        right_margin_enabled = self.bind_key_to_toggle(
            'period', _('Right Margin:'), 'right_margin_enabled',
            _('Show a vertical line at a specified column number')
        )
        right_margin = Gtk.SpinButton(
            adjustment=Gtk.Adjustment(80, 40, 200, 4, 20, 0)
        )
        intercom.state.right_margin.control_gobject_property(
            right_margin, 'value', 'value_changed'
        )
        intercom.state.right_margin_enabled.connect(
            lambda enabled: setattr(right_margin.props, 'sensitive', enabled)
        )
        # Focus the right margin spin button when the right margin is
        # toggled on.
        def on_right_margin_toggled(button):
            if button.props.active:
                self._stack.pin_menu(right_margin)
        right_margin_enabled.connect('toggled', on_right_margin_toggled)
        right_margin.connect('activate', lambda rm: self._stack.unpin_menu())
        self.add_extra_widget(right_margin, 19, 2, 4, 1)
        self.add_unused_keys()


class Language(Menu):
    def __init__(self, intercom, menu_stack):
        super().__init__(Menu.Side.RIGHT, intercom, menu_stack)
        self.__intercom = intercom
        self.bind_key_to_back_button('bracketright')
        scroller = Gtk.ScrolledWindow(shadow_type=Gtk.ShadowType.IN)
        self.__language_list = LanguageList()
        scroller.add(self.__language_list)
        self.add_extra_widget(scroller, 0, 0, 24, 3)
        self.focus_widget = self.__language_list
        language_handler = self.__language_list.connect(
            'selection-changed', self.__on_language_changed
        )
        def on_language_state_changed(language):
            self.__language_list.handler_block(language_handler)
            self.__language_list.language_id = language
            self.__language_list.handler_unblock(language_handler)
        intercom.state.language.connect(on_language_state_changed)
        self.__language_list.connect(
            'item-activated',
            # TODO
            lambda _view, path:
                self._stack.on_language_activated(self.__language_list, path)
        )
        self.add_unused_keys()

    @property
    def language_list(self):
        return self.__language_list

    def __on_language_changed(self, language_list):
        self.__intercom.state.language.value = language_list.language_id


class LanguageList(Gtk.IconView):
    COLUMN_ID = 0
    COLUMN_TEXT = 1

    def __init__(self, **props):
        super().__init__(
            text_column=self.COLUMN_TEXT, item_padding=0,
            item_orientation=Gtk.Orientation.HORIZONTAL, row_spacing=0, 
            activate_on_single_click=True,
            # TODO: Remove hard-coded size?
            item_width=144, **props
        )
        model = Gtk.ListStore(str, str)
        model.insert(-1, ['plain', _('Plain')])
        lang_man = GtkSource.LanguageManager.get_default()
        for lang_id in lang_man.props.language_ids:
            lang = lang_man.get_language(lang_id)
            if not lang.props.hidden:
                model.insert(-1, [lang_id, lang.props.name])
        self.props.model = model
        self.__search_string = ''
        self.__previous_selection = None
        self.__search_timeout = 0
        self.__select_item(Gtk.TreePath.new_first())

    def __select_item(self, path):
        self.set_cursor(path, None, False)
        # XXX: set_cursor should select the cursor path, but it doesn’t
        # seem to.  Is it something to do with the CellRenderer?
        self.select_path(path)

    @property
    def language_id(self):
        iters = self.get_selected_items()
        if iters:
            iter_ = self.props.model.get_iter(iters[0])
            (id_,) = self.props.model.get(iter_, self.COLUMN_ID)
            return id_
        else:
            return 'plain'

    @language_id.setter
    def language_id(self, id_):
        path = None
        def check_id_and_set_path(model, try_path, try_iter):
            nonlocal path
            if self.props.model.get(try_iter, self.COLUMN_ID)[0] == id_:
                path = try_path
                return True  # Stop iteration
            else:
                return False
        self.props.model.foreach(check_id_and_set_path)
        if path is not None:
            self.select_path(path)
            self.scroll_to_path(path, False, 0, 0)

    def do_key_press_event(self, event):
        if Gtk.IconView.do_key_press_event(self, event):
            return True
        else:
            codepoint = Gdk.keyval_to_unicode(event.keyval)
            if event.keyval == Gdk.KEY_BackSpace:
                if self.__search_string:
                    self.__search_string = self.__search_string[:-1]
                self.__update_search()
                return True
            elif event.keyval in (Gdk.KEY_Return, Gdk.KEY_KP_Enter):
                return False
            elif codepoint:
                if not self.__search_string:
                    iters = self.get_selected_items()
                    if iters:
                        self.__previous_selection = iters[0]
                self.__search_string += chr(codepoint)
                self.__update_search()
                return True
            else:
                return False

    def __update_search(self):
        """
        Update the selection and reset the search timeout.

        Update the selection to the first item that starts (case-
        insensitively) with self.__search_string.  If
        self.__search_string is empty, reset the selection to
        self.__previous_selection instead.  Then reset the search
        timeout."""
        def callback(model, path, iter_):
            name, = model.get(iter_, self.COLUMN_TEXT)
            name = name.casefold().replace(' ', '')
            search_string = self.__search_string.casefold().replace(' ', '')
            if name.startswith(search_string):
                self.__select_item(path)
                return True
            else:
                return False
        if self.__search_string:
            self.props.model.foreach(callback)
        elif self.__previous_selection is not None:
            self.__select_item(self.__previous_selection)
        if self.__search_timeout:
            GLib.source_remove(self.__search_timeout)
        self.__search_timeout = GLib.timeout_add(
            1000, self.__clear_search_string
        )

    def __clear_search_string(self):
        self.__search_string = ''
        self.__search_timeout = 0


class Find(make_menu_class(Gtk.Grid)):
    __ROW_ENTRIES = 0
    __ROW_KEYBOARD = 1
    # TODO: Switch columns for right-to-left languages.
    __COLUMN_FIND = 0
    __COLUMN_REPLACE = 1
    __N_COLUMNS = 2

    def __init__(self, intercom, menu_stack, **props):
        super().__init__(Menu.Side.RIGHT, intercom, menu_stack, **props)
        self.__find_entry = Gtk.Entry()
        self.__find_entry.props.placeholder_text = _('Text to find')
        self.focus_widget = self.__find_entry
        self.attach(
            self.__find_entry, self.__COLUMN_FIND, self.__ROW_ENTRIES, 1, 1
        )
        self.__replace_entry = Gtk.Entry()
        self.__replace_entry.props.placeholder_text = _('Replacement text')
        self.attach(
            self.__replace_entry, self.__COLUMN_REPLACE, self.__ROW_ENTRIES,
            1, 1
        )
        self.__key_grid = Gtk.Grid()
        self._set_grid(self.__key_grid)
        self.attach(
            self.__key_grid, 0, self.__ROW_KEYBOARD, self.__N_COLUMNS, 1
        )
        self.bind_key_to_widget('u', _('Text to find'), self.__find_entry)
        self.bind_key_to_widget(
            'o', _('Replacement text'), self.__replace_entry
        )
        self.bind_key_to_back_button('bracketright')
        search = self.bind_key_to_callback(
            'j', _('Find next'),
            lambda: intercom.command.find_next(self.__find_entry.props.text)
        )
        @intercom.command.find_next.enabled_connect
        def on_search_enabled_disabled(state):
            search.props.sensitive = state
        replace = self.bind_key_to_callback(
            'k', _('Replace'),
            lambda: intercom.command.replace(self.__replace_entry.props.text)
        )
        @intercom.command.replace.enabled_connect
        def on_replace_enabled_disabled(state):
            replace.props.sensitive = state
        self.bind_key_to_toggle('l', _('Match case'), 'find_match_case')
        self.bind_key_to_toggle(
            'semicolon', _('Regular expression'), 'find_regex'
        )
        replace_all = self.bind_key_to_callback(
            'comma', _('Replace all'),
            lambda:
                intercom.command.replace_all(
                    self.__find_entry.props.text,
                    self.__replace_entry.props.text
                )
        )
        @intercom.command.replace_all.enabled_connect
        def on_replace_all_enabled_disabled(state):
            replace_all.props.sensitive = state
        self.add_unused_keys()
