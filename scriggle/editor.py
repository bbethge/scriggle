from gettext import gettext as _

import gi
from gi.repository import GLib, GObject, Gio, Gdk, Gtk, GtkSource

from .editor_support import (
    id_from_language, guess_file_language, guess_file_indentation_settings,
    language_from_id, MenuRevealer, get_modeline_info, SaveIndicator,
    SearchManager
)
from .intercom import Intercom
from . import py_async_glib


class Editor(Gtk.ApplicationWindow):
    __ROW_TEXT_VIEW = 0
    __ROW_SAVE_INDICATOR = 1
    __ROW_MENU = 2

    def __init__(self, file=None, **props):
        super().__init__(
            default_width=640, default_height=480, icon_name='scriggle',
            **props
        )
        self.__file = file
        self.__close_after_save = False
        self.__intercom = Intercom()

        self.__grid = Gtk.Grid(orientation=Gtk.Orientation.VERTICAL)
        self.add(self.__grid)

        self.__save_indicator = SaveIndicator()
        self.__grid.attach(
            self.__save_indicator, 0, self.__ROW_SAVE_INDICATOR, 1, 1
        )

        self.__menu_revealer = MenuRevealer(self.__intercom)
        self.__grid.attach(self.__menu_revealer, 0, self.__ROW_MENU, 1, 1)

        self.__scroller = Gtk.ScrolledWindow(
            shadow_type=Gtk.ShadowType.IN, expand=True
        )

        self.__source_view = GtkSource.View(
            expand=True, monospace=True, show_right_margin=True,
            smart_backspace=True, auto_indent=True,
            smart_home_end=GtkSource.SmartHomeEndType.BEFORE
        )
        self.__scroller.add(self.__source_view)
        self.buffer.connect('mark-set', self.__on_mark_set)
        self.buffer.connect('modified-changed', self.__on_modified_changed)
        self.__intercom.command.undo.enabled = False
        self.buffer.connect('notify::can-undo', self.__forward_can_undo)
        self.__on_cursor_position_changed(self.buffer.get_start_iter())

        SearchManager(self.__intercom, self.__source_view)

        self.__intercom.command.undo.connect(self.on_undo)
        self.__intercom.command.cut.connect(self.on_cut)
        self.__intercom.command.copy.connect(self.on_copy)
        self.__intercom.command.paste.connect(self.on_paste)
        self.__intercom.command.up.connect(self.on_up)
        self.__intercom.command.left.connect(self.on_left)
        self.__intercom.command.down.connect(self.on_down)
        self.__intercom.command.right.connect(self.on_right)
        self.__intercom.command.left_word.connect(self.on_left_word)
        self.__intercom.command.right_word.connect(self.on_right_word)
        self.__intercom.command.selection_start_up.connect(
            self.on_selection_start_up
        )
        self.__intercom.command.selection_start_down.connect(
            self.on_selection_start_down
        )
        self.__intercom.command.selection_end_up.connect(
            self.on_selection_end_up
        )
        self.__intercom.command.selection_end_down.connect(
            self.on_selection_end_down
        )
        self.__intercom.command.selection_start_left.connect(
            self.on_selection_start_left
        )
        self.__intercom.command.selection_start_right.connect(
            self.on_selection_start_right
        )
        self.__intercom.command.selection_end_left.connect(
            self.on_selection_end_left
        )
        self.__intercom.command.selection_end_right.connect(
            self.on_selection_end_right
        )
        self.__intercom.command.select_all.connect(self.on_select_all)
        self.__intercom.command.new.connect(self.on_new)
        self.__intercom.command.close.connect(self.on_close)
        self.__intercom.command.open.connect(self.on_open)
        self.__intercom.command.save.connect(self.on_save)
        self.__intercom.state.auto_indent.register_for_gobject_property(
            self.__source_view, 'auto-indent'
        )
        self.__intercom.state.use_spaces.register_for_gobject_property(
            self.__source_view, 'insert_spaces_instead_of_tabs'
        )
        self.__intercom.state.tab_width.register_for_gobject_property(
            self.__source_view, 'tab_width'
        )
        self.__intercom.state.indent_width.register_for_gobject_property(
            self.__source_view, 'indent_width'
        )
        self.__intercom.state.line_numbers.register_for_gobject_property(
            self.__source_view, 'show_line_numbers'
        )
        self.__intercom.state.language.register(
            self.get_language, self.set_language
        )
        hl_cur_line = self.__intercom.state.highlight_current_line
        hl_cur_line.register_for_gobject_property(self.__source_view,
                                                  'highlight-current-line')
        hl_brac = self.__intercom.state.highlight_matching_brackets
        hl_brac.register_for_gobject_property(self.buffer,
                                              'highlight-matching-brackets')
        self.__intercom.state.soft_wrap.register(
            lambda: self.__source_view.props.wrap_mode != Gtk.WrapMode.NONE,
            lambda w:
                setattr(self.__source_view.props, 'wrap_mode',
                        Gtk.WrapMode.WORD_CHAR if w else Gtk.WrapMode.NONE)
        )
        self.__source_view.connect(
            'notify::wrap-mode',
            lambda sv, p: self.__intercom.state.soft_wrap.notify()
        )
        self.buffer.connect(
            'notify::language',
            lambda b, p: self.__intercom.state.language.notify()
        )
        rm_en = self.__intercom.state.right_margin_enabled
        rm_en.register_for_gobject_property(self.__source_view,
                                            'show-right-margin')
        self.__intercom.state.right_margin.register_for_gobject_property(
            self.__source_view, 'right-margin-position'
        )

        py_async_glib.launch(self.__attach_scroller, file)

    async def __attach_scroller(self, file):
        if file is None:
            self.__grid.attach(self.__scroller, 0, self.__ROW_TEXT_VIEW, 1, 1)
        else:
            await self.__load(file)
        self.buffer.connect('changed', self.__on_buffer_changed)
        self.buffer.set_modified(False)

    async def __load(self, file):
        progress_grid = Gtk.Grid(orientation=Gtk.Orientation.HORIZONTAL)
        label = Gtk.Label(_('Loading…'))
        progress_grid.add(label)
        cancel_button = Gtk.Button.new_from_stock(Gtk.STOCK_CANCEL)
        progress_grid.add(cancel_button)
        self.__grid.attach(progress_grid, 0, self.__ROW_TEXT_VIEW, 1, 1)
        cancellable = Gio.Cancellable()
        source_file = GtkSource.File(location=file)
        loader = GtkSource.FileLoader(buffer=self.buffer, file=source_file)
        cancel_button.connect('clicked', lambda b: cancellable.cancel())
        # TODO: Show progress
        try:
            await loader.load_pyasync(GLib.PRIORITY_DEFAULT, cancellable, None)
        except GLib.Error as error:
            message = (
                _('Unable to load “{filename}”: {message}')
                .format(
                    filename=loader.props.location.get_path(),
                    message=error.message
                )
            )
            dialog = Gtk.MessageDialog(
                self,
                Gtk.DialogFlags.MODAL | Gtk.DialogFlags.DESTROY_WITH_PARENT,
                Gtk.MessageType.ERROR, Gtk.ButtonsType.CLOSE, message
            )
            dialog.run()
            dialog.destroy()
        lang_man = GtkSource.LanguageManager.get_default()
        lang = guess_file_language(self.filename, self.buffer, lang_man)
        self.__intercom.state.language.value = id_from_language(lang)
        use_spaces, indent_width = guess_file_indentation_settings(self.buffer)
        self.__intercom.state.use_spaces.value = use_spaces
        if use_spaces:
            self.__intercom.state.indent_width.value = indent_width
        modeline_opts = get_modeline_info(self.buffer)
        for opt, value in modeline_opts.items():
            setattr(self.__source_view.props, opt, value)
        self.__grid.remove(progress_grid)
        self.__grid.attach(self.__scroller, 0, self.__ROW_TEXT_VIEW, 1, 1)
        self.__scroller.show_all()
        self.__source_view.grab_focus()
        self.buffer.place_cursor(self.buffer.get_start_iter())

    @property
    def buffer(self):
        return self.__source_view.props.buffer

    @property
    def filename(self):
        if self.__file is None:
            return None
        else:
            return self.__file.get_path()

    @GObject.Property(
        type=bool, flags=GObject.ParamFlags.READABLE, default=False
    )
    def modified(self):
        return self.buffer.get_modified()

    @property
    def menu_revealer(self):
        return self.__menu_revealer

    def do_window_state_event(self, event):
        if event.changed_mask & Gdk.WindowState.FOCUSED:
            self.__menu_revealer.window_focus_changed(
                bool(event.new_window_state & Gdk.WindowState.FOCUSED)
            )
        return Gtk.ApplicationWindow.do_window_state_event(self, event)

    def do_key_press_event(self, event):
        if self.__menu_revealer.key_event(event):
            return True
        else:
            return Gtk.ApplicationWindow.do_key_press_event(self, event)

    def do_key_release_event(self, event):
        if self.__menu_revealer.key_event(event):
            return True
        else:
            return Gtk.ApplicationWindow.do_key_release_event(self, event)

    def __on_mark_set(self, _buffer, location, mark):
        if mark.props.name == 'insert':
            self.__on_cursor_position_changed(location)

    def __on_modified_changed(self, _buffer):
        self.notify('modified')

    def __on_buffer_changed(self, buffer):
        location = buffer.get_iter_at_mark(buffer.get_insert())
        self.__on_cursor_position_changed(location)

    def __on_cursor_position_changed(self, location):
        self.__menu_revealer.cursor_position = (
            location.get_line(), location.get_line_offset()
        )

    def get_language(self):
        return id_from_language(self.buffer.props.language)

    def set_language(self, language_id):
        self.buffer.props.language = language_from_id(language_id)

    def on_tab_width_changed(self, tab_width):
        self.__source_view.props.tab_width = tab_width

    def on_new(self):
        if self.props.application is not None:
            self.props.application.activate()

    def do_delete_event(self, _event):
        self.on_close()
        return True

    def on_close(self):
        if self.buffer.get_modified():
            DISCARD = 0
            SAVE = 1
            dialog = Gtk.MessageDialog(
                self, Gtk.DialogFlags.MODAL, Gtk.MessageType.QUESTION,
                Gtk.ButtonsType.NONE,
                _('Save changes to {} before closing?')
                .format(self.props.title)
            )
            dialog.add_buttons(
                _('Close without Saving'), DISCARD,
                _('Cancel'), Gtk.ResponseType.CANCEL, _('Save'), SAVE
            )
            dialog.set_default_response(SAVE)
            response = dialog.run()
            dialog.destroy()
            if response in [
                    Gtk.ResponseType.CANCEL, Gtk.ResponseType.DELETE_EVENT
            ]:
                return
            elif response == SAVE:
                self.__close_after_save = True
                self.on_save()
                return
        self.__finish_close()

    def __finish_close(self):
        """
        Finish closing the window.

        Remove ‘self’ from its application and destroy it.
        """
        if self.props.application is not None:
            self.props.application.remove_window(self)
        self.destroy()

    def on_open(self):
        self.props.application.show_open_dialog(self)

    def on_save(self):
        py_async_glib.launch(self.__save)

    async def __save(self):
        if self.__file is None:
            chooser = Gtk.FileChooserNative.new(
                _('Save As…'), self, Gtk.FileChooserAction.SAVE, None, None
            )
            response = chooser.run()
            if response != Gtk.ResponseType.ACCEPT:
                return
            filename = chooser.get_filename()
            self.__file = Gio.File.new_for_path(filename)
        assert self.__file is not None
        assert self.__file.get_path() is not None
        source_file = GtkSource.File(location=self.__file)
        cancellable = Gio.Cancellable()
        cancel_handler = self.__save_indicator.connect(
            'cancel-clicked', lambda b: cancellable.cancel()
        )
        self.__save_indicator.show_all()
        saver = GtkSource.FileSaver(buffer=self.buffer, file=source_file)
        # TODO: Show progress
        try:
            await saver.save_pyasync(GLib.PRIORITY_DEFAULT, cancellable, None)
        except GLib.Error as error:
            message = (
                _('Unable to save file “{filename}”: {message}')
                .format(
                    filename=saver.props.location.get_path(),
                    message=error.message
                )
            )
            dialog = Gtk.MessageDialog(
                self, Gtk.DialogFlags.DESTROY_WITH_PARENT,
                Gtk.MessageType.ERROR,
                # XXX: Is ‘Close’ confusing (it
                # will not close the file)?
                Gtk.ButtonsType.CLOSE, message
            )
            dialog.run()
            dialog.destroy()
            self.__save_terminated(cancel_handler)
            return
        self.__save_terminated(cancel_handler)
        self.buffer.set_modified(False)
        if self.__close_after_save:
            self.__finish_close()
            self.__close_after_save = False

    def __save_terminated(self, cancel_handler):
        """Called when a save is finished or aborted due to error."""
        self.__save_indicator.hide()
        self.__save_indicator.disconnect(cancel_handler)

    def __forward_can_undo(self, buffer_, pspec):
        self.__intercom.command.undo.enabled = buffer_.props.can_undo

    def on_undo(self):
        self.buffer.undo()

    def on_cut(self):
        clip = self.get_clipboard(Gdk.SELECTION_CLIPBOARD)
        self.buffer.cut_clipboard(clip, True)

    def on_copy(self):
        clip = self.get_clipboard(Gdk.SELECTION_CLIPBOARD)
        self.buffer.copy_clipboard(clip)

    def on_paste(self):
        clip = self.get_clipboard(Gdk.SELECTION_CLIPBOARD)
        self.buffer.paste_clipboard(clip, None, True)

    def on_up(self):
        self.__source_view.emit(
            'move-cursor', Gtk.MovementStep.DISPLAY_LINES, -1, False
        )

    def on_down(self):
        self.__source_view.emit(
            'move-cursor', Gtk.MovementStep.DISPLAY_LINES, 1, False
        )

    def on_left(self):
        self.__source_view.emit(
            'move-cursor', Gtk.MovementStep.VISUAL_POSITIONS, -1, False
        )

    def on_right(self):
        self.__source_view.emit(
            'move-cursor', Gtk.MovementStep.VISUAL_POSITIONS, 1, False
        )

    def on_left_word(self):
        self.__source_view.emit(
            'move-cursor', Gtk.MovementStep.WORDS, -1, False
        )

    def on_right_word(self):
        self.__source_view.emit(
            'move-cursor', Gtk.MovementStep.WORDS, 1, False
        )

    def on_select_all(self):
        self.__source_view.emit('select-all', True)

    def __move_selection_edge(self, edge, iter_transform):
        """
        Move an edge (start or end) of the selection.

        If ‘edge’ is 0, move the start; if ‘edge’ is 1, move the end.
        iter_transform is a function that takes a Gtk.TextIter,
        optionally modifies it, and returns True if it was modified.
        If the motion would go past the other edge of the selection,
        only go as far as the other edge, leaving no selection.
        """
        sel = self.buffer.get_selection_bounds()
        if sel == ():
            ins = self.buffer.get_iter_at_mark(self.buffer.get_insert())
            sel = [ins, ins.copy()]
        if iter_transform(sel[edge]):
            if sel[0].compare(sel[1]) == 1:
                sel[edge].assign(sel[1-edge])
            self.buffer.select_range(sel[0], sel[1])

    def on_selection_start_down(self):
        self.__move_selection_edge(0, self.__source_view.forward_display_line)

    def on_selection_start_up(self):
        self.__move_selection_edge(0, self.__source_view.backward_display_line)

    def on_selection_end_down(self):
        self.__move_selection_edge(1, self.__source_view.forward_display_line)

    def on_selection_end_up(self):
        self.__move_selection_edge(1, self.__source_view.backward_display_line)

    def on_selection_start_left(self):
        self.__move_selection_edge(
            0, lambda it: self.__source_view.move_visually(it, -1)
        )

    def on_selection_start_right(self):
        self.__move_selection_edge(
            0, lambda it: self.__source_view.move_visually(it, 1)
        )

    def on_selection_end_left(self):
        self.__move_selection_edge(
            1, lambda it: self.__source_view.move_visually(it, -1)
        )

    def on_selection_end_right(self):
        self.__move_selection_edge(
            1, lambda it: self.__source_view.move_visually(it, 1)
        )
