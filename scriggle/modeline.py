import re

import gi
from gi.repository import Gtk


modeline_identifier_re = re.compile(
    r'(?P<prefix>(?:^|[ \t])vim?|[ \t](?:Vim|ex)):[ \t]*(?P<rest>.*)'
)
def parse_vim_modeline(line):
    ml_start_match = modeline_identifier_re.search(line)
    if not ml_start_match:
        return {}
    prefix = ml_start_match['prefix'].lstrip()
    rest = ml_start_match['rest'].rstrip()
    set_match = re.match(r'set?[ \t]+(?P<rest>.*)(?<!\\):', rest)
    if set_match:
        ml_type = 2
        rest = set_match['rest'].rstrip().replace(r'\:', ':')
        opts = rest.split()
    else:
        ml_type = 1
        opts = re.split(r':|[ \t]+', rest)
    sts = None
    sw = None
    ts = None
    result = {}
    for opt_str in opts:
        opt, eq, val = opt_str.partition('=')
        if eq == '':
            if opt in ['ai', 'autoindent']:
                result['auto-indent'] = True
            elif opt in ['noai', 'noautoindent']:
                result['auto-indent'] = False
            elif opt in ['et', 'expandtab']:
                result['insert-spaces-instead-of-tabs'] = True
            elif opt in ['noet', 'noexpandtab']:
                result['insert-spaces-instead-of-tabs'] = False
            elif opt in ['nu', 'number']:
                result['show-line-numbers'] = True
            elif opt in ['nonu', 'nonumber']:
                result['show-line-numbers'] = False
            elif opt in ['cul', 'cursorline']:
                result['highlight-current-line'] = True
            elif opt in ['nocul', 'nocursorline']:
                result['highlight-current-line'] = False
            elif opt in ['sm', 'showmatch']:
                result['highlight-matching-bracket'] = True
            elif opt in ['nosm', 'noshowmatch']:
                result['highlight-matching-bracket'] = False
            elif opt == 'wrap':
                result['wrap-mode'] = Gtk.WrapMode.WORD_CHAR
            elif opt == 'nowrap':
                result['wrap-mode'] = Gtk.WrapMode.NONE
        else:
            if opt in ['ts', 'tabstop']:
                n = int(val)
                if 1 <= n <= 32:
                    result['tab-width'] = n
            if opt in ['sw', 'shiftwidth']:
                n = int(val)
                if 0 <= n <= 32:
                    sw = n
            if opt in ['wiw', 'winwidth']:
                n = int(val)
                if n == 0:
                    result['show-right-margin'] = False
                else:
                    result['show-right-margin'] = True
                    result['right-margin-position'] = n
    if sw is not None:
        if sw > 0:
            result['indent-width'] = sw
        elif sw == 0:
            if 'tab-width' in result:
                result['indent-width'] = result['tab-width']
            else:
                result['indent-width'] = 8
    return result
