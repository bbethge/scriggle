from gettext import gettext as _
import re

import gi
from gi.repository import GObject, Gdk, Gio, Gtk, GtkSource

from .menu_stack import MenuStack
from .modeline import parse_vim_modeline


N_CHARS_TO_SCAN_FOR_FILE_TYPE = 1024
N_LINES_TO_SCAN_FOR_INDENTATION = 1000
N_LINES_TO_SCAN_FOR_VIM_MODELINES = 5
DEFAULT_TAB_WIDTH = 8
ALLOWED_INDENT_WIDTHS = [2, 3, 4, 8]


def language_from_id(language_id):
    lang_man = GtkSource.LanguageManager.get_default()
    if language_id == 'plain':
        return None
    else:
        return lang_man.get_language(language_id)


def id_from_language(language):
    if language is None:
        return 'plain'
    else:
        return language.props.id


def guess_file_language(path, buffer, language_manager):
    data_str = buffer.get_text(
        buffer.get_start_iter(),
        buffer.get_iter_at_offset(N_CHARS_TO_SCAN_FOR_FILE_TYPE),
        True
    )
    data = data_str.encode('utf-8')
    content_type, type_uncertain = Gio.content_type_guess(path, data)
    if type_uncertain:
        content_type = None
    # Assume Python 3 for ambiguous Python files.  GIO assumes Python 2
    # as if that were the “normal” Python version and then has the gall
    # to say it’s not uncertain.
    if content_type == 'text/x-python' and not path.endswith('.py2'):
        content_type = 'text/x-python3'
    return language_manager.guess_language(path, content_type)


def guess_file_indentation_settings(buffer):
    line_start = buffer.get_start_iter()
    next_line_start = line_start.copy()
    for i in range(N_LINES_TO_SCAN_FOR_INDENTATION):
        is_last_line = not next_line_start.forward_line()
        line = buffer.get_slice(line_start, next_line_start, True)
        if line.startswith('\t'):
            return False, DEFAULT_TAB_WIDTH
        elif line.startswith(' '):
            indent_width = len(re.match(r' +', line)[0])
            if indent_width in ALLOWED_INDENT_WIDTHS:
                return True, indent_width
        if is_last_line:
            break
        line_start.assign(next_line_start)
    return False, DEFAULT_TAB_WIDTH


def get_modeline_info(buffer):
    info = {}
    line_start = buffer.get_start_iter()
    next_line_start = line_start.copy()
    for i in range(N_LINES_TO_SCAN_FOR_VIM_MODELINES):
        is_last_line = not next_line_start.forward_line()
        line = buffer.get_slice(line_start, next_line_start, True)
        info.update(parse_vim_modeline(line))
        if is_last_line:
            break
    end_of_top_lines = next_line_start
    line_start = buffer.get_end_iter()
    line_start.backward_lines(N_LINES_TO_SCAN_FOR_VIM_MODELINES)
    while line_start.compare(end_of_top_lines) < 0:
        line_start.forward_line()
    next_line_start = line_start.copy()
    while True:
        is_last_line = not next_line_start.forward_line()
        line = buffer.get_slice(line_start, next_line_start, True)
        info.update(parse_vim_modeline(line))
        if is_last_line:
            break
    return info


class SearchManager:
    def __init__(self, intercom, view):
        self.__intercom = intercom
        self.__view = view
        self.__buffer = view.props.buffer
        self.__buffer.connect('mark-set', self.__mark_set_cb)
        self.__search_context = GtkSource.SearchContext.new(
            self.__buffer, None
        )
        self.__search_settings = self.__search_context.props.settings
        self.__intercom.command.find_next.connect(self.__on_find_next)
        self.__intercom.command.replace.connect(self.__on_replace)
        self.__intercom.command.replace.enabled = False
        self.__intercom.command.replace_all.connect(self.__on_replace_all)
        self.__intercom.command.replace_all.enabled = False
        self.__intercom.state.find_match_case.register(
            lambda: self.__search_settings.props.case_sensitive,
            lambda value:
                setattr(self.__search_settings.props, 'case_sensitive', value)
        )
        self.__intercom.state.find_regex.register(
            lambda: self.__search_settings.props.regex_enabled,
            lambda value:
                setattr(self.__search_settings.props, 'regex_enabled', value)
        )

    def __on_find_next(self, text_to_find):
        py_async_glib.launch(self.__find_next_async, text_to_find)

    async def __find_next_async(self, text_to_find):
        self.__search_settings.props.search_text = text_to_find
        await self.__find_current_search_text()

    async def __find_current_search_text(self):
        self.__intercom.command.find_next.enabled = False
        self.__intercom.command.replace.enabled = False
        self.__intercom.command.replace_all.enabled = False
        matched, start, end, _wrapped = (
            await self.__search_context.forward_pyasync(
                self.__buffer.get_iter_at_mark(self.__buffer.get_insert()), None
            )
        )
        if matched:
            # First argument will be the insertion point -- we want that
            # to be the end of the match.
            self.__buffer.select_range(end, start)
            self.__selection_changed_since_find = False
            # Scroll to the start *and* the end to get as much as
            # possible of the match into view.
            # See the documentation of Gtk.TextView.scroll_to_iter for a
            # possible issue about computing line heights.  Since we
            # will almost certainly have fixed line heights, this
            # shouldn’t be a problem.
            self.__view.scroll_to_iter(start, 0.1, False, 0, 0)
            self.__view.scroll_to_iter(end, 0.1, False, 0, 0)
        self.__intercom.command.replace.enabled = matched
        self.__intercom.command.replace_all.enabled = matched
        self.__intercom.command.find_next.enabled = True

    def __mark_set_cb(self, buffer_, iter_, mark):
        if mark.props.name in ('insert', 'selection_bound'):
            self.__intercom.command.replace.enabled = False

    def __replacement_is_valid(self, replacement):
        result = True
        if self.__search_settings.props.regex_enabled:
            try:
                GLib.regex_check_replacement(replacement)
            except GLib.Error as error:
                if error.matches(
                        GLib.regex_error_quark(), GLib.RegexError.REPLACE
                ):
                    result = False
                else:
                    raise
        return result

    def __on_replace(self, replacement):
        py_async_glib.launch(self.__replace_async, replacement)

    async def __replace_async(self, replacement):
        # FIXME: Show user why replacement is invalid.
        if self.__replacement_is_valid(replacement):
            start, end = self.__buffer.get_selection_bounds()
            self.__search_context.replace2(start, end, replacement, -1)
            await self.__find_current_search_text()

    def __on_replace_all(self, text_to_find, replacement):
        py_async_glib.launch(
            self.__replace_all_async, text_to_find, replacement
        )

    async def __replace_all_async(self, text_to_find, replacement):
        # XXX: Could the user conceivably edit the buffer while this is
        #      happening?
        # FIXME: Show user why replacement is invalid.
        if self.__replacement_is_valid(replacement):
            self.__search_settings.props.search_text = text_to_find
            self.__intercom.command.find_next.enabled = False
            self.__intercom.command.replace.enabled = False
            self.__intercom.command.replace_all.enabled = False
            self.__buffer.begin_user_action()
            while True:
                matched, start, end, wrapped = (
                    await self.__search_context.forward_pyasync(
                        self.__buffer.get_start_iter(), None
                    )
                )
                if (not matched) or wrapped:
                    break
                self.__search_context.replace2(start, end, replacement, -1)
                await py_async_glib.idle()
            self.__buffer.end_user_action()
            self.__intercom.command.find_next.enabled = True


class SaveIndicator(Gtk.Grid):
    def __init__(self, **props):
        super().__init__(
            orientation=Gtk.Orientation.HORIZONTAL, no_show_all=True
        )

        save_label = Gtk.Label(_('Saving…'))
        self.add(save_label)

        save_cancel_button = Gtk.Button.new_with_label(_('Cancel'))
        save_cancel_button.connect(
            'clicked', lambda b: self.emit('cancel-clicked')
        )
        self.add(save_cancel_button)

    @GObject.Signal
    def cancel_clicked(self):
        pass


class MenuRevealer(Gtk.Revealer):
    def __init__(self, intercom):
        super().__init__(transition_type=Gtk.RevealerTransitionType.SLIDE_UP)
        self.__intercom = intercom
        self.__menu_stack = MenuStack(intercom, self)
        self.add(self.__menu_stack)
        self.__menu_pinned = False
        self.__control_pressed = False

    @property
    def menu_pinned(self):
        return self.__menu_pinned

    @menu_pinned.setter
    def menu_pinned(self, value):
        if not value and not self.__control_pressed:
            self.props.reveal_child = False
        self.__menu_pinned = value

    @property
    def left_menu(self):
        return self.__menu_stack.left_menu

    @property
    def right_menu(self):
        return self__menu_stack.right_menu

    @property
    def cursor_position(self):
        return self.__menu_stack.cursor_position

    @cursor_position.setter
    def cursor_position(self, position):
        self.__menu_stack.cursor_position = position

    def key_event(self, event):
        if event.keyval in [Gdk.KEY_Control_L, Gdk.KEY_Control_R]:
            if event.type == Gdk.EventType.KEY_PRESS:
                return self.__on_control_pressed(event.keyval)
            else:
                return self.__on_control_released()
        elif event.state & Gdk.ModifierType.CONTROL_MASK:
            return self.__menu_stack.key_event(event)
        else:
            return False

    def __on_control_pressed(self, keyval):
        self.__control_pressed = True
        self.props.reveal_child = True
        if not self.__menu_pinned:
            if keyval == Gdk.KEY_Control_L:
                self.__menu_stack.show_menu_immediately(
                    self.__menu_stack.right_menu
                )
            else:
                self.__menu_stack.show_menu_immediately(
                    self.__menu_stack.left_menu
                )

    def __on_control_released(self):
        self.__control_pressed = False
        if not self.__menu_pinned:
            self.props.reveal_child = False

    def window_focus_changed(self, focused):
        """
        Notify ‘self’ of focus changes in its toplevel window

        Treat loss of window focus the same as Ctrl being released.
        """
        if not focused:
            self.__on_control_released()
